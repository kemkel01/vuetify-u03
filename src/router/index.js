import { createRouter, createWebHashHistory } from 'vue-router'
import TeamView from '@/views/TeamView.vue'
import ForschungView from '@/views/ForschungView'
import BotGarten from '@/views/BotGarten.vue'
import TFeld from '@/views/TFeld.vue'
import WannseeView from '@/views/WannseeView.vue'
import DataView from '@/views/DataView.vue'

const routes = [
//Zuweisung der einzelnen ViewDateien zu den jeweiligen URLS wichtig ist hierbei der name um den in der navbar 
// zu verwenden 
  {
    path: '/',
    name: 'home',
    component: ForschungView
  },
  {
    path: '/forschung',
    name: 'forschung',
    component: ForschungView
  },
  {
    path: '/team',
    name: 'team',
    component: TeamView
  },
  {
    path: '/wannsee',
    name: 'wannsee',
    component: WannseeView
  },
  {
    path: '/tfeld',
    name: 'tfeld',
    component: TFeld
  },
  {
    path: '/botgarten',
    name: 'botgarten',
    component: BotGarten
  },
  {
    path:'/data',
    name: 'data',
    component: DataView
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
